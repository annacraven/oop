//  ---------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Header file for all Controller programs.
//  Created by Anna Craven on Dec. 14, 2019
// ----------------------------------------------------------------------------

#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

#include "inventory.hpp"

class Controller {
public:
	Controller() {}
	void go(string infile, string outfile);
};

#endif