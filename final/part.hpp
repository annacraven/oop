//  ---------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Header file for all Part programs.
//  Created by Anna Craven on Dec. 14, 2019
// ----------------------------------------------------------------------------

#ifndef PART_HPP
#define PART_HPP

#include <string>
#include <iostream> // std::cout
#include <iomanip> // std::setprecision
#include <algorithm> //std::max()
#include <vector>
using namespace std;

#include "exceptions.hpp"

class Part {
	// friend class Test; // uncomment for testing
private:
	string name;
	int quantity;
	double price;
public:
	// uncomment for testing
	// Part(string n, int q, double p) : name(n), quantity(q), price(p) {}
	Part() =default;

	ostream& print(ostream& out) const; // prints the part out
	istream& read(istream& in); // reads a Part from stream in
	// returns true if first part's name is alphabetically before second's name 
	static bool compareByName(Part i, Part j) { return i.name < j.name; }
	// returns true if the two parts have the same name
	static bool isSameName(Part i, Part j) { return i.name == j.name; }
	// merges the parts into one
	void merge(vector<Part> pv);
};

#endif
