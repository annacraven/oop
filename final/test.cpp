// //  ---------------------------------------------------------------------------
// //  CPSC 427: Object-Oriented Programming
// //  Test program for repair program.                               test.cpp
// //  Created by Anna Craven on Dec. 14, 2019
// // ----------------------------------------------------------------------------

// #include "test.hpp"

// void Test::run() {
// 	cout << "_____________________________TESTING____________________________\n";
// 	testPart();
// 	testInventory();
// 	cout << "_____________________________END____________________________\n";
// }

// void Test::testPart() {
// 	cout << "--------------------Testing Part--------------------\n";

// 	cout << "-----Test 1: Price prints as 2 decimal places-----\n";
// 	Part p("name", 1, 2.00);
// 	p.print(cout);
// 	cout << "Expected: 2.00\n";
// 	Part p1("name1", 2, 3);
// 	p1.print(cout);
// 	cout << "Expected: 3.00\n";
// 	Part p2("name2", 3, 2.999);
// 	p2.print(cout);
// 	cout << "Expected: 3.00\n";

// 	cout << "-----Test 2: Compare by name works-----\n";
// 	Part i("Bob", 1, 1);
// 	Part j("Alice", 2, 2);
// 	(!Part::compareByName(i, j)) ? cout << "PASSED\n" : cout << "FAILED\n\n\n";
// 	Part k("Charlie", 3, 10);
// 	(Part::compareByName(j, k)) ? cout << "PASSED\n" : cout << "FAILED\n\n\n";
// 	(Part::compareByName(i, k)) ? cout << "PASSED\n" : cout << "FAILED\n\n\n";

// 	cout << "-----Test 2: isSameName works-----\n";
// 	(!Part::isSameName(i, j)) ? cout << "PASSED\n" : cout << "FAILED\n\n\n";
// 	Part l("Bob", 2, 17);
// 	(Part::isSameName(i, l)) ? cout << "PASSED\n" : cout << "FAILED\n\n\n";

// 	testPartMerge();
// }

// void Test::testPartMerge() {
// 	cout << "-----Test 3: merge works-----\n";
// 	Part p3;
// 	vector<Part> pv;
// 	pv.push_back(Part("J", 2, 3));
// 	pv.push_back(Part("J", 2, 5.98));
// 	pv.push_back(Part("J", 10, 6.77));
// 	p3.merge(pv);

// 	(p3.price == 6.77) ? cout << "PASSED\n" : cout << "FAILED\n\n\n";
// 	(p3.quantity == 14) ? cout << "PASSED\n" : cout << "FAILED\n\n\n";

// 	Part p4;
// 	vector<Part> pv2;
// 	Part p4a("Anna", 3, 2.5);
// 	Part p4b("Anna", 2, 1);
// 	pv2.push_back(p4a);
// 	pv2.push_back(p4b);
// 	p4.merge(pv2);

// 	(p4.name == "Anna") ? cout << "PASSED\n" : cout << "FAILED\n\n\n";
// 	(p4.price == 2.5) ? cout << "PASSED\n" : cout << "FAILED\n\n\n";
// 	(p4.quantity == 5) ? cout << "PASSED\n" : cout << "FAILED\n\n\n";
// }

// void Test::testInventory() {
// 	cout << "--------------------Testing Inventory--------------------\n";
// 	cout << "-----Test 1: Read in valid input-----\n";
// 	Inventory i;
// 	i.readIFile("io/valid.in");
// 	i.print(cout);
// 	(i.parts.size() == 9) ? cout << "PASSED\n" : cout << "FAILED\n";

// 	invalidInput();

// 	cout << "-----Test 3: Inventory sort-----\n";
// 	i.sortByName();
// 	(i.parts[1].name == "Anna") ? cout << "PASSED\n" : cout << "FAILED\n\n\n";

// 	cout << "-----Test 4: Inventory merge-----\n";
// 	Inventory i2;
// 	i.merge(i2);
// 	(i.parts.size() == 9) ? cout << "PASSED\n" : cout << "FAILED\n\n\n";
// 	(i2.parts.size() == 6) ? cout << "PASSED\n" : cout << "FAILED\n\n\n";
// }

// void Test::invalidInput() {
// 	cout << "-----Test 2: Invalid filename-----\n";
// 	tryInput("asdf");

// 	cout << "-----Test 3: Invalid input-----\n";
// 	cout << "Test 3a: string quantity\n";
// 	tryInput("io/invalid.in");

// 	cout << "Test 3b: float quantity\n";
// 	tryInput("io/invalid5.in");

// 	cout << "Test 3c: negative quantity\n";
// 	tryInput("io/invalid1.in");

// 	cout << "Test 3d: string price\n";
// 	tryInput("io/invalid2.in");

// 	cout << "Test 3e: negative price\n";
// 	tryInput("io/invalid3.in");
// }

// void Test::tryInput(string filename) {
// 	try {
// 		Inventory i;
//    i.readIFile(filename);
// 	}
// 	catch (IOerror& e) {
// 		e.what();
// 	}
// }