// -----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Implementation file for all Controller programs.
//  Created by Anna Craven on Dec. 14, 2019
// -----------------------------------------------------------------------------

#include "controller.hpp"

void Controller::go(string infile, string outfile) {
	// create object to represent old inventory
	Inventory oldI;
	try {
		oldI.readIFile(infile);
	}
	catch (IOerror& e) {
		throw;
	}

	// attempt to open output file
	// see notes.txt for explanation of why this is here instead of line 36
	ofstream fout(outfile);
	if (!fout) {
		throw IOerror("Unable to open file " + outfile);
	}

	// sort by part name
	oldI.sortByName();

	// create new empty Inventory
	Inventory newI;

	// merge old Inventory into new
	oldI.merge(newI);

	// write new Inventory to file
	newI.print(fout);
	fout.close();
}
