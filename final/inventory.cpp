// -----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Implementation file for all Inventory programs.
//  Created by Anna Craven on Dec. 14, 2019
// -----------------------------------------------------------------------------

#include "inventory.hpp"

void Inventory::readIFile(string filename) {
	// open file for reading
	ifstream fin(filename);
	if (!fin) {
		throw IOerror("Couldn't open input " + filename);
	}

	// read each part
	do {
		try {
			Part p;
			p.read(fin);
			if (!fin.fail()) parts.push_back(p);
		}
		catch (IOerror& e) {
			throw;
		}
	} while (fin.good());

	fin.close();
}

void Inventory::print(ostream& out) const {
	if (parts.empty()) out << "Empty inventory\n";
	for (auto i = parts.begin(); i != parts.end(); ++i) {
		(*i).print(out);
	}
}

void Inventory::sortByName() {
	using Comp = bool (*) (Part, Part);
	Comp cptr;
	cptr = Part::compareByName;
	std::sort(parts.begin(), parts.end(), cptr);
}

void Inventory::merge(Inventory& target) const {
	auto i = parts.begin();
	auto j = parts.begin();
	auto end = parts.end();
	while (i != end) {
		while (j != end and Part::isSameName(*i, *j)) {
			j++;
		}
		
		Part p;
		p.merge(vector<Part>(i, j));
		target.parts.push_back(p);
		i = j;
	}
}
