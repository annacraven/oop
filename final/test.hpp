#ifndef TEST_HPP
#define TEST_HPP

#include "part.hpp"
#include "inventory.hpp"
#include "controller.hpp"
#include "exceptions.hpp"

// class to run tests
class Test {
public:
	Test()=default;
	void run();

	// part functions
	void testPart();
	void testPartMerge();

	// inventory functions
	void testInventory();
	void invalidInput();
	// tries to call Inventory constructor with the given filename
	void tryInput(string filename);
};

#endif