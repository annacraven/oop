// -----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Implementation file for all Part programs.
//  Created by Anna Craven on Dec. 14, 2019
// -----------------------------------------------------------------------------

#include "part.hpp"

ostream& Part::print(ostream& out) const {
	out << name << " " << quantity << " ";
	out << fixed << setprecision(2) << price << endl;
	return out;
}

istream& Part::read(istream& in) {
	in >> name;
	if (in.fail()) {
		return in;
	}
	in >> quantity;
	if (in.fail() or quantity < 0) {
		throw IOerror("Invalid quantity");
	}
	in >> price;
	if (in.fail() or price < 0) {
		throw IOerror("Invalid price");
	}
	return in;
}

void Part::merge(vector<Part> pv) {
	name = pv.begin()->name;
	quantity = 0;
	price = -1;
	cout << "Merging:\n";
	for (auto i = pv.begin(); i != pv.end(); ++i) {
		(*i).print(cout);
		quantity += i->quantity;
		price = max(price, i->price);
	}
	cout << "End merge\n";
}
