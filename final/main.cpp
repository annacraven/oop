//  ---------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Main program for repair program.                               main.cpp
//  Created by Anna Craven on Dec. 14, 2019
// ----------------------------------------------------------------------------

#include "test.hpp"
#include "controller.hpp"

int main(int argc, char** argv) {
	// uncomment to run tests
	// Test t;
	// t.run();

	// process command line arguments
	if (argc != 3) {
		cout << "Usage: ./repair <input file> <output file>\n";
		return 1;
	}

	// instantiate the controller class
	Controller c;

	// call go()
	try { 
		c.go(argv[1], argv[2]);
	}
	// catch IOerror exception
	catch (IOerror& e) {
		// call what() and terminate the program
		e.what();
		return 1;
	}

	return 0;
}
