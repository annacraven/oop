//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Header file for all Exception classes.
//  Created by Anna Craven on Dec. 14, 2019
// -----------------------------------------------------------------------------
#ifndef EXCEPTION_HPP
#define EXCEPTION_HPP

class IOerror {
	string msg;

public:
	IOerror(string s): msg(s) {}
	void what() const { cerr << msg << endl; }
};

#endif
