//  ---------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Header file for all Inventory programs.
//  Created by Anna Craven on Dec. 14, 2019
// ----------------------------------------------------------------------------

#ifndef INVENTORY_HPP
#define INVENTORY_HPP

#include <vector>
#include <fstream>
#include <algorithm> // std::sort
#include "part.hpp"

class Inventory {
	// friend class Test; // uncomment for testing
private:
	vector<Part> parts; // vector of parts

public:
	Inventory() {}
	// functions for reading and writing inventory files
	void readIFile(string filename); // reads given inventory file
	void print(ostream& out) const;

	// functions for repairing the parts list
	void sortByName(); // sorts parts alphabetically by part name
	// merges each block of entries for the same part, and pushes the resulting
	// part onto the target Inventory object
	void merge(Inventory& target) const;
};

#endif