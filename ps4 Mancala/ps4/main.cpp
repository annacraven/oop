//  ---------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Main program for testing Kalah game.                               main.cpp
//  Created by Anna Craven on Oct. 4, 2019
// ----------------------------------------------------------------------------

#include "board.hpp"
#include "tools.hpp"

void run(int numHouses, int numSeeds);
void testPit(int numSeeds);
void testBoard(int numHouses, int numSeeds);

// ----------------------------------------------------------------------------
int main(int argc, char** argv) {
    banner();
    // process command line arguments
    if (argc != 3) {
        fatal("Usage: testKalah <numHouses> <numSeeds>");
    }
    const int numHouses = atoi(argv[1]);
    const int numSeeds = atoi(argv[2]);
    run(numHouses, numSeeds);
    bye();
    return 0;
}

// runs the test of Pit and Board
void run(int numHouses, int numSeeds) {
    cout << "Testing Pit\n";
    cout << "====================================\n";
    testPit(numSeeds);

    cout << "\n\nTesting Board\n";
    cout << "====================================\n";
    testBoard(numHouses, numSeeds);
}

// function to test Pit construction and methods
void testPit(int numSeeds) {
    cout << "---Testing Pit constructor\n";
    Pit new_pit = Pit(N, 1, "N1", numSeeds, nullptr, nullptr);
    cout << "Arguments: N, 1, N1, " << numSeeds << " nullptr, nullptr\n";
    cout << new_pit << "\n";
    cout << "---Testing Pit:scoop(). Expected: " << numSeeds << ". Actual: ";
    cout << new_pit.scoop() << "\n";
    cout << "---Testing Pit:isEmpty(). Expected: 1. Actual: ";
    cout << new_pit.isEmpty() << "\n";
    cout << "---Testing Pit:drop(). Return nothing.\n";
    new_pit.drop();
    cout << "---Testing Pit:isEmpty(). Expected: 0. Actual: ";
    cout << new_pit.isEmpty() << "\n";
    cout << "---Testing Pit:printContents() Expected: 1. Actual: ";
    new_pit.printContents();
    cout << "---Testing Pit:isStore(). Expected: 0. Actual: ";
    cout << new_pit.isStore() << "\n";
}

// function to test Board construction and methods
void testBoard(int numHouses, int numSeeds) {
    cout << "---Testing Board constructor\n";
    cout << "Arguments: " << numHouses << " houses, " << numSeeds << " seeds\n";
    Board new_board = Board(numHouses, numSeeds);
    cout << new_board << "\n";

    cout << "--Test that opposite was set correctly\n";
    new_board.printPitAndOpposite();
}