Anna Craven
CPSC 427
README.txt for explaing testKalah files
Created on October 11, 2019

Files:
player.hpp
- defines a player enum type, N for North player, S for South player
- overload the << operator to print out North or South

pit.hpp
- defines a Pit class, which represents a single pit in a Kalah board
- has two print functions. 
  + debugPrint prints out all the pit information.
	+ << operator calls debugPrint.
	+ printContents() just prints the contents of the pit (the number of seeds)

board.hpp
- defines a Board class, which represents a board for the game Kalah
- overloads the << operator to call Board::print()

board.cpp

main.cpp
- makes sure that exactly 3 arguments are given.
- I didn't validate input beyond that, because Kerim said that we would not need to.
- defines two test functions, testPit() and testBoard.
- each test states the function being test, the arguments (if any)
- some tests have an expected result, and the expected result should be the same as the actual result
