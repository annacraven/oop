// -----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Board class:  a board with pits, for a game of Kalah.
//  Implementation file for all Board programs.                  board.cpp
//  Created by Anna Craven on October 4, 2019
// -----------------------------------------------------------------------------
#include "board.hpp"

ostream& Board::print(ostream& out) const {
    out << "-----------Printing Board-----------\n";
    for (int i = 0; i < size; i++) {
        out << pits[i];
    }
    return out;
}

// function to test as the spec states
// "visit each house and print the label of the house and its opposite"
void Board::printPitAndOpposite() const {
    for (int i = 0; i < size; i++) {
        cout << "Pit: " << pits[i].label() << "\n";
        cout << "Opposite: " << pits[i].oppositePit() << "\n\n";
    }
}
