//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Header file for all Board programs. board.hpp Created by Anna Craven on Oct.
//  4, 2019
// -----------------------------------------------------------------------------

#include "pit.hpp"

class Board {
  private:
    int numHouses;
    int numSeeds;
    Pit* pits;
    int size; // size of pits array. 2 * (numHouses + 1)

  public:
    Board(int _numHouses, int _numSeeds) {
        numHouses = _numHouses;
        numSeeds = _numSeeds;
        size = 2 * (numHouses + 1);
        pits = new Pit[size];

        // keep track of which slot you are currently filling in in pits
        int i = 0;
        // create south houses
        while (i < numHouses) {
            int number = numHouses - i;
            string string_label = "S" + to_string(number);
            Pit* next = &pits[i + 1];
            Pit* opposite = &pits[size - 2 - i];
            pits[i] = Pit(S, number, string_label, numSeeds, next, opposite);
            i++;
        }
        // create south store. contents is 0, opposite is nullptr
        pits[i] = Pit(S, 0, "S0", 0, &pits[i + 1], nullptr);
        i++;
        // create north houses
        while (i < size - 1) {
            int number = size - i - 1;
            string string_label = "N" + to_string(number);
            Pit* next = &pits[i + 1];
            Pit* opposite = &pits[size - 2 - i];
            pits[i] = Pit(N, number, string_label, numSeeds, next, opposite);
            i++;
        }
        // create north store, and set its next to the start of the pits array
        pits[i] = Pit(N, 0, "N0", 0, &pits[0], nullptr);
    }
    ~Board() { delete[] pits; }

    ostream& print(ostream& out) const;
    void printPitAndOpposite() const;
};

// overload << operator to call Board's print function
inline ostream& operator<<(ostream& out, const Board& board) {
    return board.print(out);
}
