//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Header file for all Pit programs. pit.hpp
//  Created by Anna Craven on Oct. 4, 2019
// -----------------------------------------------------------------------------

#include "player.hpp"
#include "tools.hpp"

class Pit {
  private:
    Player owner;     // which Player owns the pit? N or S
    int number;       // what is the number of the pit? 0-6
    string str_label; // a label for the pit, e.g. N01
    int contents;     // the number of seeds currently in the pit
    Pit* next;        // a pointer to the next pit, proceeding counterclockwise
    Pit* opposite;    // a pointer to the pit opposite this one

  public:
    Pit() = default;
    Pit(Player _owner, int _number, string _str_label, int initial_contents,
        Pit* _next, Pit* _opposite) {
        owner = _owner;
        number = _number;
        str_label = _str_label;
        contents = initial_contents;
        next = _next;
        opposite = _opposite;
    }

    // return the number of seeds in the pit, leaving the pit empty
    inline int scoop();
    // drop a seed into the pit
    inline void drop() { contents++; }
    // return true if the pit is empty
    inline bool isEmpty() const { return contents == 0; }
    // print the pit contents
    inline void printContents() const { cout << contents << "\n"; }
    // print all the print information
    inline ostream& debugPrint(ostream& out) const;
    // returns true if the pit is a store
    inline bool isStore() const { return number == 0; }
    // returns the next pit
    inline Pit* nextPit() const { return next; }
    // returns the label
    inline string label() const { return str_label; }
    // returns the label of the opposite pit, or null if it has no opposite
    inline string oppositePit() const;
};

inline int Pit::scoop() {
    int temp = contents;
    contents = 0;
    return temp;
}

// returns the label of the opposite pit, or null if it has no opposite
inline string Pit::oppositePit() const {
    if (opposite == nullptr) {
        return "nullptr";
    } else {
        return opposite->str_label;
    }
}

inline ostream& Pit::debugPrint(ostream& out) const {
    out << "-------Printing Pit-------\n";
    out << "Owner: " << owner << "\n";
    out << "Number: " << number << "\n";
    out << "Label: " << str_label << "\n";
    if (next != nullptr) {
        out << "Next: " << next->str_label << "\n";
    } else {
        out << "Next: nullptr\n";
    }
    if (opposite != nullptr) {
        out << "Opposite: " << opposite->str_label << "\n";
    } else {
        out << "Opposite: nullptr\n";
    }
    out << "Contents: " << contents << "\n";
    return out;
}

// overload << operator to call Pit class's print function
inline ostream& operator<<(ostream& out, const Pit& pit) {
    return pit.debugPrint(out);
}