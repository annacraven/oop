//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Header file for Agent type enum. Created by Anna Craven on Nov. 4, 2019
// -----------------------------------------------------------------------------

#ifndef AGENT_TYPE_HPP
#define AGENT_TYPE_HPP

#include "tools.hpp"

// an enum to represent the type of an agent, either human or machine
enum AgentType { HUMAN, MACHINE };

// overload << operator to print either human or machine
inline ostream& operator<<(ostream& out, const AgentType& at) {
    if (at == HUMAN) {
        out << "Human";
    } else if (at == MACHINE) {
        out << "MACHINE";
    } else {
        out << "Unknown Type";
    }
    return out;
}

#endif
