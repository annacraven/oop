//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Implementation file for all Referee programs.
//  Created by Anna Craven on Oct. 27, 2019
// -----------------------------------------------------------------------------

#include "referee.hpp"

void Referee::playGame() {
    cout << "Welcome to Kalah!\n";
    while (not game.isOver()) {
        cout << "----------\n";
        cout << game.getActivePlayer() << "'s  turn:\n";

        int move = current->chooseMove();
        if (move == -1) {
            cout << "Quitting game at user's request\n";
            return;
        }
        processMove(move);
    }

    game.endGame();
    showResults();
}

void Referee::processMove(int move) {
    cout << game.getActivePlayer() << " playing move " << move << "\n";
    game.doMove(move);
    // check if there was a capture
    if (game.captured()) {
        cout << "Captured " << game.captured() << " seeds\n";
    }
    // check if player gets to choose a move again
    if (game.again()) {
        cout << game.getActivePlayer() << " gets another turn\n";
    } else {
        switchAgent();
        cout << "Turn is over\n";
    }
}

void Referee::showResults() {
    cout << "----------\n"
         << "Results:\n";
    game.printBoard();
    int n_score = game.score(N);
    int s_score = game.score(S);
    cout << "North's Score: " << n_score << "\n";
    cout << "South's Score: " << s_score << "\n";

    // display winner
    if (n_score > s_score) {
        cout << "North wins!\n";
    } else if (s_score > n_score) {
        cout << "South wins!\n";
    } else {
        cout << "Tie!\n";
    }
}

Agent* Referee::makeAgent(string agentType, const Game& game, Player pl) {
    if (agentType == "human") {
        return new HumanAgent(game, pl);
    } else if (agentType == "machine") {
        return new MachineAgent(game, pl);
    } else {
        fatal("Invalid agent type");
    }
    return nullptr;
}

void Referee::switchAgent() {
    if (current == North) {
        current = South;
    } else {
        current = North;
    }
}