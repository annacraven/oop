//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Implementation file for all HumanAgent programs.
//  Created by Anna Craven on Nov 3, 2019
// -----------------------------------------------------------------------------
#include "HumanAgent.hpp"

int HumanAgent::chooseMove() {
    game.printBoard();
    string input = "0";
    int move = 0;
    while (not game.isValidMove(move)) {
        cout << "Please enter a valid move (pit number) for " << pl;
        cout << " (q to quit):\n";
        cin >> input;
        if (input == "q") {
            return -1;
        }
        try {
            move = stoi(input);
        } catch (const std::invalid_argument& ia) {
            move = 0;
        }
    }
    return move;
}