//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Header file for all Machine Agent programs.
//  Created by Anna Craven on Nov 3, 2019
// -----------------------------------------------------------------------------
#ifndef MACHINE_AGENT_HPP
#define MACHINE_AGENT_HPP

#include "agent.hpp"

class MachineAgent : public Agent {
  public:
    MachineAgent(const Game& game, Player player) : Agent(game, player) {}
    virtual int chooseMove(); // chooses the first non-empty pit
    virtual ~MachineAgent() {}
};

#endif