//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Header file for all Referee programs.
//  Created by Anna Craven on Oct. 27, 2019
// -----------------------------------------------------------------------------

#ifndef REFEREE_HPP
#define REFEREE_HPP

#include "HumanAgent.hpp"
#include "MachineAgent.hpp"
#include "agent.hpp"

class Referee {
  private:
    Game game;
    Agent* South;
    Agent* North;
    Agent* current;

  public:
    Referee(int numHouses, int numSeeds, string sAgent, string nAgent)
        : game(numHouses, numSeeds) {
        South = makeAgent(sAgent, game, S);
        North = makeAgent(nAgent, game, N);
        current = South; // South goes first
    }
    ~Referee() {
        delete South;
        delete North;
    }

    // do moves until the game is over
    void playGame();
    // tells the user what is happening with the move, and calls game.doMove
    void processMove(int move);
    // show scores and winner
    void showResults();
    // makes either HumanAgent or MachineAgent
    Agent* makeAgent(string agentType, const Game& game, Player pl);
    // switch the current agent from north to south or south to north
    void switchAgent();
};

#endif