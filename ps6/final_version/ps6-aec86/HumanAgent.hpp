//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Header file for all Human Agent programs.
//  Created by Anna Craven on Nov 3, 2019
// -----------------------------------------------------------------------------
#ifndef HUMAN_AGENT_HPP
#define HUMAN_AGENT_HPP

#include "agent.hpp"

class HumanAgent : public Agent {
  public:
    HumanAgent(const Game& game, Player player) : Agent(game, player) {}
    virtual int chooseMove();
    virtual ~HumanAgent() {}
};

#endif