//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Header file for all Agent programs.
//  Created by Anna Craven on Nov 3, 2019
// -----------------------------------------------------------------------------
#ifndef AGENT_HPP
#define AGENT_HPP

#include "game.hpp"

class Agent {
  protected:
    const Game& game;
    const Player pl;
    Agent(const Game& _game, Player player) : game(_game), pl(player) {}

  public:
    virtual ~Agent() = default;
    virtual int chooseMove() = 0;
};

#endif