_______________________________________________________________________________
TESTS FROM PS5

++++++TEST 1++++++
kalah 6 4 when run on the sample input file results in the same final board
as shown in the sample output file.

./kalah 6 4 human human < sample.in

---------------------------------------------------------------
        Anna Craven
        CPSC 427/527
        Sun Nov  3 2019 21:15:38
---------------------------------------------------------------
Welcome to Kalah!
----------
.
.
.
North's  turn:

         N1    N2    N3    N4    N5    N6
        [ 6]  [ 0]  [ 9]  [ 2]  [ 0]  [ 1]
N0 [ 2]                                    [ 12] S0
         S6    S5    S4    S3    S2    S1
        [ 0]  [ 0]  [ 0]  [ 8]  [ 8]  [ 0]
Please enter a valid move (pit number) for North (q to quit):
Quitting game at user's request
Game over -- goodbye!

---------------------------------------------------------------
Normal termination.

This is the same final board as shown in the sample output file, so this test passes.



++++++TEST 2++++++
kalah 3 2 gives correct answers when tested on an input file mytest.in
of your choosing that leads to a complete game.

./kalah 3 2 human human < mytest.in

---------------------------------------------------------------
        Anna Craven
        CPSC 427/527
        Sun Nov  3 2019 21:18:41
---------------------------------------------------------------
Welcome to Kalah!
----------
.
.
.
----------
Results:

         N1    N2    N3
        [ 0]  [ 0]  [ 0]
N0 [ 10]                  [ 2] S0
         S3    S2    S1
        [ 0]  [ 0]  [ 0]
North's Score: 10
South's Score: 2
North wins!
Game over -- goodbye!

---------------------------------------------------------------
Normal termination.

This is what I expected the final board to look like, so this tests passes.
