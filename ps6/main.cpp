//  ---------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Main program for testing Kalah game with agents.
//  Created by Anna Craven on Nov. 4, 2019
// ----------------------------------------------------------------------------

#include "referee.hpp"

void run(int numHouses, int numSeeds, string sAgent, string nAgent);

// Test functions from p4
// Commented out because they are not relevant to the functionality
// void testPit(int numSeeds);
// void testBoard(int numHouses, int numSeeds);

// ----------------------------------------------------------------------------
int main(int argc, char** argv) {
    banner();
    // process command line arguments
    if (argc != 5) {
        fatal("Usage: testKalah <numHouses> <numSeeds> <S Agent> <N Agent>");
    }
    const int numHouses = atoi(argv[1]);
    const int numSeeds = atoi(argv[2]);
    run(numHouses, numSeeds, argv[3], argv[4]);
    bye();
    return 0;
}

void run(int numHouses, int numSeeds, string sAgent, string nAgent) {
    Referee ref = Referee(numHouses, numSeeds, sAgent, nAgent);
    ref.playGame();
    cout << "Game over -- goodbye!\n";

    // cout << "Testing Pit\n";
    // cout << "====================================\n";
    // testPit(numSeeds);

    // cout << "\n\nTesting Board\n";
    // cout << "====================================\n";
    // testBoard(numHouses, numSeeds);

}

// function to test Pit construction and methods
// void testPit(int numSeeds) {
//     cout << "---Testing Pit constructor\n";
//     Pit new_pit = Pit(N, 1, "N1", numSeeds, nullptr, nullptr);
//     cout << "Arguments: N, 1, N1, " << numSeeds << " nullptr, nullptr\n";
//     cout << new_pit << "\n";
//     cout << "---Testing Pit:scoop(). Expected: " << numSeeds << ". Actual: ";
//     cout << new_pit.scoop() << "\n";
//     cout << "---Testing Pit:isEmpty(). Expected: 1. Actual: ";
//     cout << new_pit.isEmpty() << "\n";
//     cout << "---Testing Pit:drop(). Return nothing.\n";
//     new_pit.drop();
//     cout << "---Testing Pit:isEmpty(). Expected: 0. Actual: ";
//     cout << new_pit.isEmpty() << "\n";
//     cout << "---Testing Pit:printContents() Expected: 1. Actual: ";
//     new_pit.printContents();
//     cout << "---Testing Pit:isStore(). Expected: 0. Actual: ";
//     cout << new_pit.isStore() << "\n";
// }

// // function to test Board construction and methods
// void testBoard(int numHouses, int numSeeds) {
//     cout << "---Testing Board constructor\n";
//     cout << "Arguments: " << numHouses << " houses, " << numSeeds << " seeds\n";
//     Board new_board = Board(numHouses, numSeeds);
//     cout << new_board << "\n";

//     cout << "--Test that opposite was set correctly\n";
//     new_board.printPitAndOpposite();
// }
