//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Header file for Player enum. player.hpp Created by Anna Craven on Oct. 4,
//  2019
// -----------------------------------------------------------------------------

#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "tools.hpp"

// an enum to represent Player in a Kalah game, either North or South
enum Player { N, S };

// overload << operator to print either North or South
inline ostream& operator<<(ostream& out, const Player& player) {
    if (player == N) {
        out << "North";
    } else if (player == S) {
        out << "South";
    } else {
        out << "Unknown Player";
    }
    return out;
}

inline Player operator!(Player p) {
	if (p == N) {
		return S;
	}
	else if (p == S) {
		return N;
	}
	cout << "Unknown Player";
	return p;
}

#endif
