//                  -*- mode:c++; tab-width:4 -*-
// File: aboutme.cpp ----------------------------------------------------------
// The C++ aboutme implementation files.
// A program that prompts the user to enter their first name, last name, and
// year of birth. The program then prints out the first name, last name, and
// age. Author: Anna Craven. Created September 2019.
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "tools.hpp"

// Promts user to enter name
string get_name() {
    string first_name;
    cout << "Please enter your first name: ";
    cin >> first_name;

    string last_name;
    cout << "Please enter your last name: ";
    cin >> last_name;

    return first_name + " " + last_name;
}

// Prompts user to enter year of birth
int get_year() {
    int year;
    cout << "Please enter the year of your birth: ";
    cin >> year;

    if (year < 0) {
        fatal("Input must be > 0");
    } else if (cin.good()) {
        return year;
    } else {
        fatal("Invalid Input");
    }
    return 1;
}

// Returns the current year
int calc_current_year() {
    time_t rawtime;
    time(&rawtime);

    struct tm* timeinfo;
    timeinfo = localtime(&rawtime);

    int current_year = timeinfo->tm_year + 1900;
    return current_year;
}

// Prints user data
int print_data(string name, int year) {
    int current_year = calc_current_year();
    int age = current_year - year;

    cout << name << " becomes " << age << " years old in " << current_year
         << ".";

    return 0;
}

// Prompts user to enter name and birth year, then prints name and age
void run() {
    string name = get_name();
    int year = get_year();

    print_data(name, year);
}

int main() {
    banner();
    run();
    bye();
}
