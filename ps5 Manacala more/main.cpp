//  ---------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Main program for testing Kalah game.                               main.cpp
//  Created by Anna Craven on Oct. 4, 2019
// ----------------------------------------------------------------------------

#include "referee.hpp"
#include "tools.hpp"

void run(int numHouses, int numSeeds);
void testPit(int numSeeds);
void testBoard(int numHouses, int numSeeds);

// ----------------------------------------------------------------------------
int main(int argc, char** argv) {
    banner();
    // process command line arguments
    if (argc != 3) {
        fatal("Usage: testKalah <numHouses> <numSeeds>");
    }
    const int numHouses = atoi(argv[1]);
    const int numSeeds = atoi(argv[2]);
    run(numHouses, numSeeds);
    bye();
    return 0;
}

// runs the test of Pit and Board
void run(int numHouses, int numSeeds) {
    Referee ref = Referee(numHouses, numSeeds);
    ref.playGame();
    cout << "Game over -- goodbye!\n";
}
