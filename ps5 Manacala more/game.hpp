//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Header file for all Game programs. 
//  Created by Anna Craven on Oct. 27, 2019
// -----------------------------------------------------------------------------

#ifndef GAME_HPP
#define GAME_HPP

#include "player.hpp"
#include "board.hpp"

class Game {
	private:
		Player activePlayer; // the player whose turn it is
		Board board; // a board on which to play the game
		int capturedSeeds; // if a capture was performed, how many seeds
		bool anotherTurn; // true if the active player gets to go again

	public:
		Game(int numHouses, int numSeeds) : board(numHouses, numSeeds) {
			activePlayer = S; // South goes first, by rules of Kalah
			anotherTurn = false;
		}

		bool isValidMove(int move);
		void doMove(int move);
		// switches activePlayer from North to South, or vice versa
		void switchTurn();
		// returns true if the game is over
		// i.e., if all the houses of one player are empty
		bool isOver();
		void endGame();
		// return the player whose turn it is
		Player getActivePlayer() const { return activePlayer; }
		// print board
		void printBoard() const { cout << board; }
		// get score for given player
		int score(Player p) { return board.storeContents(p); }
		// get number of captured seeds
		int captured() const { return capturedSeeds; }
		// check whether the current player gets to play again
		bool again() const { return anotherTurn; }

};

#endif