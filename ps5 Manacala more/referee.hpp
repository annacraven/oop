//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Header file for all Referee programs. 
//  Created by Anna Craven on Oct. 27, 2019
// -----------------------------------------------------------------------------

#ifndef REFEREE_HPP
#define REFEREE_HPP

#include "game.hpp"

class Referee {
	private:
		Game game;

	public:
		Referee(int numHouses, int numSeeds) : game(numHouses, numSeeds) { }

		// do moves until the game is over
		void playGame();
		// tells the user what is happening with the move, and calls game.doMove
		void processMove(int move);
		// show scores and winner
		void showResults();
		
};

#endif