//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Implementation file for all Game programs. 
//  Created by Anna Craven on Oct. 27, 2019
// -----------------------------------------------------------------------------

#include "game.hpp"

// check is the given house is a valid move
// i.e., not store, and non-empty
bool Game::isValidMove(int move) {
	if (move < 0) {
		return false;
	}
	Pit* pit = board.findPit(activePlayer, move);
	return !pit->isEmpty() && !pit->isStore();
}

void Game::doMove(int move) {
	Pit* lastPit = board.sow(activePlayer, move);
	// if lastPit is the player's own store, then player gets another turn
	if (lastPit->isOwnStore(activePlayer)) {
		anotherTurn = true;
		return;
	}
	else {
		anotherTurn = false;
	}

	// a capture is performed if applicable
	capturedSeeds = board.doCapture(activePlayer, lastPit);

	// update game logic
	switchTurn();
	
}

void Game::switchTurn() {
	if (activePlayer == N) {
		activePlayer = S;
	}
	else {
		activePlayer = N;
	}
}

// returns true if all the houses of one player are empty
bool Game::isOver() {
	return board.allEmpty(N) or board.allEmpty(S);
}

void Game::endGame() {
	// move seeds
	board.clearHouses();
}

