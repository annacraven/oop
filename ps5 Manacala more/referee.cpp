//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Implementation file for all Referee programs. 
//  Created by Anna Craven on Oct. 27, 2019
// -----------------------------------------------------------------------------

#include "referee.hpp"

void Referee::playGame() {
	cout << "Welcome to Kalah!\n";
	while (not game.isOver()) {
		cout << "----------\n";
		cout << game.getActivePlayer() << "'s  turn:\n";
		game.printBoard();
		// get user input
		string input = "0";
		int move = 0;
		while (not game.isValidMove(move)) {
			cout << "Please enter a valid move (pit number) for " << game.getActivePlayer();
			cout << " (q to quit):\n";
			cin >> input;
			if (input == "q") {
				cout << "Quitting game at user's request\n";
				return;
			}
			move = stoi(input);
		}
		processMove(move);
	}

	game.endGame();
	showResults();
}

void Referee::processMove(int move) {
	cout << game.getActivePlayer() << " playing move " << move << "\n";
	game.doMove(move);
	// check if there was a capture
	if (game.captured()) {
		cout << "Captured " << game.captured() << " seeds\n";
	}
	// check if player gets to choose a move again
	if (game.again()) {
		cout << game.getActivePlayer() << " gets another turn\n";
	}
	else {
		cout << "Turn is over\n";
	}
}

void Referee::showResults() {
	cout << "----------\n" << "Results:\n";
	game.printBoard();
	int n_score = game.score(N);
	int s_score = game.score(S);
	cout << "North's Score: " << n_score << "\n";
	cout << "South's Score: " << s_score << "\n";

	// display winner
	if (n_score > s_score) {
		cout << "North wins!\n";
	}
	else if (s_score > n_score) {
		cout << "South wins!\n";
	}
	else {
		cout << "Tie!\n";
	}

}
