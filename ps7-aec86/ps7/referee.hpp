//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Header file for all Referee programs.
//  Created by Anna Craven on Oct. 27, 2019
// -----------------------------------------------------------------------------

#ifndef REFEREE_HPP
#define REFEREE_HPP

#include "agent.hpp"

class Referee {
  private:
    Game& game;
    Agent* South;
    Agent* North;
    Agent* current;
    vector<Snapshot> undoStack;
    vector<Snapshot> redoStack;

    // processes non integer moves
    void processCommand();
    // tells the user what is happening with the move, and calls game.doMove
    void processMove(int move);
    // show scores and winner
    void showResults();
    // switch the current agent from north to south or south to north
    void switchAgent();
    // undo the most recent move
    void undo();
    // redo the most recent move
    void redo();
    // saves the game to a file
    void doSave(string filename) const;

  public:
    Referee(Game* g, Agent* sAgent, Agent* nAgent) : game(*g) {
        South = sAgent;
        North = nAgent;
        if (game.getActivePlayer() == N) {
            current = North;
        } else {
            current = South;
        }
    }

    // do moves until the game is over
    void playGame();
};

#endif