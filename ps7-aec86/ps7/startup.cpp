//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Implementation file for all Startup programs.
//  Created by Anna Craven on Dec. 4, 2019
// -----------------------------------------------------------------------------

#include "startup.hpp"

void Startup::go(int& argc, char**& argv) {
    if (argc != 5 and strcmp(argv[1], "-f")) {
        fatal("Usage: kalah <numHouses> <numSeeds> <S Agent> <N Agent>, or "
              "kalah -f <filename>");
    }

    Game* game;
    string sAgent;
    string nAgent;
    if (argc == 5) {
        const int numHouses = atoi(argv[1]);
        const int numSeeds = atoi(argv[2]);
        sAgent = argv[3];
        nAgent = argv[4];
        if (numHouses < 0 or numSeeds < 0) {
            fatal("Must provide positive integers");
        }
        // create game
        game = new Game(numHouses, numSeeds);
    }
    // read in file and create game
    else {
        // read file
        const char* filename = argv[2];
        ifstream fin(filename);
        if (!fin)
            fatal("Error: couldn't open input %s\n", filename);

        int numHouses;
        fin >> numHouses;
        if (fin.fail() or numHouses < 0) {
            fatal("Error: invalid file content for numHouses");
        }

        fin >> sAgent;
        if (fin.fail() or (sAgent != "human" and sAgent != "machine")) {
            fatal("Error: invalid file content for south agent");
        }
        fin >> nAgent;
        if (fin.fail() or (nAgent != "human" and nAgent != "machine")) {
            fatal("Error: invalid file content for north agent");
        }

        string activePlayer;
        fin >> activePlayer;
        if (fin.fail() or (activePlayer != "S" and activePlayer != "N")) {
            fatal("Error: invalid file content for player");
        }

        vector<int> seeds;
        int seed;
        do {
            fin >> seed;
            if (!fin.fail())
                seeds.push_back(seed);
        } while (fin.good());
        if (!fin.eof())
            fatal("I/O error or bad data for seeds");

        // create game
        cout << "Creating game from file\n";
        cout << "numHouses: " << numHouses << endl;
        cout << "sAgent: " << sAgent << endl;
        cout << "nAgent: " << nAgent << endl;
        cout << "activePlayer: " << activePlayer << endl;
        game = new Game(numHouses, seeds, activePlayer);
    }

    // create agents
    Agent* South = makeAgent(sAgent, game, S);
    Agent* North = makeAgent(nAgent, game, N);
    // create referee
    Referee ref = Referee(game, South, North);
    ref.playGame();

    // free
    delete South;
    delete North;
    delete game;
    cout << "Game over -- goodbye!\n";
}

Agent* Startup::makeAgent(const string& agentType, Game* game, Player pl) {
    if (agentType == "human") {
        return new HumanAgent(game, pl);
    } else if (agentType == "machine") {
        return new MachineAgent(game, pl);
    } else {
        fatal("Invalid agent type");
    }
    return nullptr;
}
