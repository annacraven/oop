//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Implementation file for all Game programs.
//  Created by Anna Craven on Oct. 27, 2019
// -----------------------------------------------------------------------------

#include "game.hpp"

// check is the given house is a valid move
// i.e., not store, and non-empty
bool Game::isValidMove(int move) const {
    Pit* pit = board.findPit(activePlayer, move);

    if (not board.isValidHouse(move)) {
        throw InvalidMoveError("Must be a valid house");
    } else if (pit->isEmpty()) {
        throw InvalidMoveError("Must be a non-empty house");
    } else {
        return true;
    }
}

void Game::doMove(int move) {
    Pit* lastPit = board.sow(activePlayer, move);
    // if lastPit is the player's own store, then player gets another turn
    if (lastPit->isOwnStore(activePlayer)) {
        anotherTurn = true;
        capturedSeeds = 0;
        return;
    } else {
        anotherTurn = false;
    }

    // a capture is performed if applicable
    capturedSeeds = board.doCapture(activePlayer, lastPit);

    // update game logic
    activePlayer = !activePlayer;
}

// returns true if all the houses of one player are empty
bool Game::isOver() { return board.allEmpty(N) or board.allEmpty(S); }

void Game::endGame() {
    // move seeds
    board.clearHouses();
}

Snapshot Game::saveState() const {
    vector<int> seeds;
    board.save(seeds);
    Snapshot s = Snapshot(activePlayer, seeds);
    return s;
}

void Game::restoreState(const Snapshot& ss) {
    board.set(ss.seeds);
    activePlayer = ss.activePlayer;
}
