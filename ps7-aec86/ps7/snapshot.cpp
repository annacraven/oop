//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Implementation file for all Snapshot programs.
//  Created by Anna Craven on Nov. 27, 2019
// -----------------------------------------------------------------------------

#include "snapshot.hpp"

ostream& Snapshot::print(ostream& out) const {
    out << activePlayer << endl;
    for (auto i = seeds.begin(); i != seeds.end(); ++i)
        out << *i << " ";
    return out;
}