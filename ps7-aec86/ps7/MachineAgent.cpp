//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Implementation file for all MachineAgent programs.
//  Created by Anna Craven on Nov 3, 2019
// -----------------------------------------------------------------------------
#include "MachineAgent.hpp"

int MachineAgent::chooseMove() { return game->getFirstNonEmptyHouse(pl); }