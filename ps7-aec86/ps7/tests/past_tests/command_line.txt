________________________________________________________________________________
TEST 1
numHouses and numSeeds
valgrind ./kalah 9 5 human human
==12715== Memcheck, a memory error detector
==12715== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==12715== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==12715== Command: ./kalah 9 5 human human
==12715==

---------------------------------------------------------------
        Anna Craven
        CPSC 427/527
        Sun Dec  8 2019 19:58:31
---------------------------------------------------------------
Welcome to Kalah!
----------
S's  turn:

         N1    N2    N3    N4    N5    N6    N7    N8    N9
        [ 5]  [ 5]  [ 5]  [ 5]  [ 5]  [ 5]  [ 5]  [ 5]  [ 5]
N0 [ 0]                                                      [ 0] S0
         S9    S8    S7    S6    S5    S4    S3    S2    S1
        [ 5]  [ 5]  [ 5]  [ 5]  [ 5]  [ 5]  [ 5]  [ 5]  [ 5]
Please enter a house number for S or a command letter z y s q h: q
Quitting game at user's request
Game over -- goodbye!

---------------------------------------------------------------
Normal termination.
==12715==
==12715== HEAP SUMMARY:
==12715==     in use at exit: 0 bytes in 0 blocks
==12715==   total heap usage: 16 allocs, 16 frees, 83,161 bytes allocated
==12715==
==12715== All heap blocks were freed -- no leaks are possible
==12715==
==12715== For lists of detected and suppressed errors, rerun with: -s
==12715== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)

________________________________________________________________________________
TEST 2
human vs human
input: 1 2 1 2 3

valgrind ./kalah 4 1 human human
==12882== Memcheck, a memory error detector
==12882== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==12882== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==12882== Command: ./kalah 4 1 human human
==12882==

---------------------------------------------------------------
        Anna Craven
        CPSC 427/527
        Sun Dec  8 2019 20:00:47
---------------------------------------------------------------
Welcome to Kalah!
----------------------------------------------------------
S's  turn:

         N1    N2    N3    N4
        [ 1]  [ 1]  [ 1]  [ 1]
N0 [ 0]                        [ 0] S0
         S4    S3    S2    S1
        [ 1]  [ 1]  [ 1]  [ 1]
Please enter a house number for S or a command letter z y s q h: 1
S playing move 1
S gets another turn
----------------------------------------------------------
S's  turn:

         N1    N2    N3    N4
        [ 1]  [ 1]  [ 1]  [ 1]
N0 [ 0]                        [ 1] S0
         S4    S3    S2    S1
        [ 1]  [ 1]  [ 1]  [ 0]
Please enter a house number for S or a command letter z y s q h: 2
S playing move 2
Captured 2 seeds
Turn is over
----------------------------------------------------------
N's  turn:

         N1    N2    N3    N4
        [ 1]  [ 1]  [ 1]  [ 0]
N0 [ 0]                        [ 3] S0
         S4    S3    S2    S1
        [ 1]  [ 1]  [ 0]  [ 0]
Please enter a house number for N or a command letter z y s q h: 1
N playing move 1
N gets another turn
----------------------------------------------------------
N's  turn:

         N1    N2    N3    N4
        [ 0]  [ 1]  [ 1]  [ 0]
N0 [ 1]                        [ 3] S0
         S4    S3    S2    S1
        [ 1]  [ 1]  [ 0]  [ 0]
Please enter a house number for N or a command letter z y s q h: 2
N playing move 2
Captured 2 seeds
Turn is over
----------------------------------------------------------
S's  turn:

         N1    N2    N3    N4
        [ 0]  [ 0]  [ 1]  [ 0]
N0 [ 3]                        [ 3] S0
         S4    S3    S2    S1
        [ 0]  [ 1]  [ 0]  [ 0]
Please enter a house number for S or a command letter z y s q h: 3
S playing move 3
Captured 2 seeds
Turn is over
----------
Results:

         N1    N2    N3    N4
        [ 0]  [ 0]  [ 0]  [ 0]
N0 [ 3]                        [ 5] S0
         S4    S3    S2    S1
        [ 0]  [ 0]  [ 0]  [ 0]
North's Score: 3
South's Score: 5
South wins!
Game over -- goodbye!

---------------------------------------------------------------
Normal termination.
==12882==
==12882== HEAP SUMMARY:
==12882==     in use at exit: 0 bytes in 0 blocks
==12882==   total heap usage: 21 allocs, 21 frees, 82,616 bytes allocated
==12882==
==12882== All heap blocks were freed -- no leaks are possible
==12882==
==12882== For lists of detected and suppressed errors, rerun with: -s
==12882== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)




________________________________________________________________________________
TEST 3
machine vs human
input: 1 2

valgrind ./kalah 4 1 machine human
==13075== Memcheck, a memory error detector
==13075== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==13075== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==13075== Command: ./kalah 4 1 machine human
==13075==

---------------------------------------------------------------
        Anna Craven
        CPSC 427/527
        Sun Dec  8 2019 20:02:39
---------------------------------------------------------------
Welcome to Kalah!
----------------------------------------------------------
S's  turn:
S playing move 1
S gets another turn
----------------------------------------------------------
S's  turn:
S playing move 2
Captured 2 seeds
Turn is over
----------------------------------------------------------
N's  turn:

         N1    N2    N3    N4
        [ 1]  [ 1]  [ 1]  [ 0]
N0 [ 0]                        [ 3] S0
         S4    S3    S2    S1
        [ 1]  [ 1]  [ 0]  [ 0]
Please enter a house number for N or a command letter z y s q h: 1
N playing move 1
N gets another turn
----------------------------------------------------------
N's  turn:

         N1    N2    N3    N4
        [ 0]  [ 1]  [ 1]  [ 0]
N0 [ 1]                        [ 3] S0
         S4    S3    S2    S1
        [ 1]  [ 1]  [ 0]  [ 0]
Please enter a house number for N or a command letter z y s q h: 2
N playing move 2
Captured 2 seeds
Turn is over
----------------------------------------------------------
S's  turn:
S playing move 3
Captured 2 seeds
Turn is over
----------
Results:

         N1    N2    N3    N4
        [ 0]  [ 0]  [ 0]  [ 0]
N0 [ 3]                        [ 5] S0
         S4    S3    S2    S1
        [ 0]  [ 0]  [ 0]  [ 0]
North's Score: 3
South's Score: 5
South wins!
Game over -- goodbye!

---------------------------------------------------------------
Normal termination.
==13075==
==13075== HEAP SUMMARY:
==13075==     in use at exit: 0 bytes in 0 blocks
==13075==   total heap usage: 18 allocs, 18 frees, 82,541 bytes allocated
==13075==
==13075== All heap blocks were freed -- no leaks are possible
==13075==
==13075== For lists of detected and suppressed errors, rerun with: -s
==13075== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)




________________________________________________________________________________
TEST 4
human vs machine
input: 2 1 3 1 2
./kalah 3 2 human machine

---------------------------------------------------------------
        Anna Craven
        CPSC 427/527
        Sun Dec  8 2019 20:03:43
---------------------------------------------------------------
Welcome to Kalah!
----------------------------------------------------------
S's  turn:

         N1    N2    N3
        [ 2]  [ 2]  [ 2]
N0 [ 0]                  [ 0] S0
         S3    S2    S1
        [ 2]  [ 2]  [ 2]
Please enter a house number for S or a command letter z y s q h: 2
S playing move 2
S gets another turn
----------------------------------------------------------
S's  turn:

         N1    N2    N3
        [ 2]  [ 2]  [ 2]
N0 [ 0]                  [ 1] S0
         S3    S2    S1
        [ 2]  [ 0]  [ 3]
Please enter a house number for S or a command letter z y s q h: 1
S playing move 1
Turn is over
----------------------------------------------------------
N's  turn:
N playing move 1
Turn is over
----------------------------------------------------------
S's  turn:

         N1    N2    N3
        [ 0]  [ 3]  [ 3]
N0 [ 1]                  [ 2] S0
         S3    S2    S1
        [ 3]  [ 0]  [ 0]
Please enter a house number for S or a command letter z y s q h: 3
S playing move 3
S gets another turn
----------------------------------------------------------
S's  turn:

         N1    N2    N3
        [ 0]  [ 3]  [ 3]
N0 [ 1]                  [ 3] S0
         S3    S2    S1
        [ 0]  [ 1]  [ 1]
Please enter a house number for S or a command letter z y s q h: 1
S playing move 1
S gets another turn
----------------------------------------------------------
S's  turn:

         N1    N2    N3
        [ 0]  [ 3]  [ 3]
N0 [ 1]                  [ 4] S0
         S3    S2    S1
        [ 0]  [ 1]  [ 0]
Please enter a house number for S or a command letter z y s q h: 2
S playing move 2
Captured 4 seeds
Turn is over
----------
Results:

         N1    N2    N3
        [ 0]  [ 0]  [ 0]
N0 [ 4]                  [ 8] S0
         S3    S2    S1
        [ 0]  [ 0]  [ 0]
North's Score: 4
South's Score: 8
South wins!
Game over -- goodbye!

---------------------------------------------------------------
Normal termination.




________________________________________________________________________________
TEST 5
machine vs machine

valgrind ./kalah 6 4 machine machine
==12699== Memcheck, a memory error detector
==12699== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==12699== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==12699== Command: ./kalah 6 4 machine machine
==12699==

---------------------------------------------------------------
        Anna Craven
        CPSC 427/527
        Sun Dec  8 2019 20:04:37
---------------------------------------------------------------
Welcome to Kalah!
----------------------------------------------------------
S's  turn:
S playing move 1
Turn is over
----------------------------------------------------------
N's  turn:
N playing move 1
Turn is over
----------------------------------------------------------
S's  turn:
S playing move 2
Turn is over
----------------------------------------------------------
N's  turn:
N playing move 2
Turn is over
----------------------------------------------------------
S's  turn:
S playing move 1
S gets another turn
----------------------------------------------------------
S's  turn:
S playing move 3
Turn is over
----------------------------------------------------------
N's  turn:
N playing move 1
N gets another turn
----------------------------------------------------------
N's  turn:
N playing move 3
Turn is over
----------------------------------------------------------
S's  turn:
S playing move 1
S gets another turn
----------------------------------------------------------
S's  turn:
S playing move 2
Captured 8 seeds
Turn is over
----------------------------------------------------------
N's  turn:
N playing move 1
N gets another turn
----------------------------------------------------------
N's  turn:
N playing move 2
Captured 8 seeds
Turn is over
----------------------------------------------------------
S's  turn:
S playing move 4
Turn is over
----------------------------------------------------------
N's  turn:
N playing move 4
Turn is over
----------------------------------------------------------
S's  turn:
S playing move 1
S gets another turn
----------------------------------------------------------
S's  turn:
S playing move 2
Captured 2 seeds
Turn is over
----------------------------------------------------------
N's  turn:
N playing move 1
N gets another turn
----------------------------------------------------------
N's  turn:
N playing move 2
Captured 2 seeds
Turn is over
----------------------------------------------------------
S's  turn:
S playing move 3
Captured 7 seeds
Turn is over
----------------------------------------------------------
N's  turn:
N playing move 3
Captured 7 seeds
Turn is over
----------
Results:

         N1    N2    N3    N4    N5    N6
        [ 0]  [ 0]  [ 0]  [ 0]  [ 0]  [ 0]
N0 [ 24]                                    [ 24] S0
         S6    S5    S4    S3    S2    S1
        [ 0]  [ 0]  [ 0]  [ 0]  [ 0]  [ 0]
North's Score: 24
South's Score: 24
Tie!
Game over -- goodbye!

---------------------------------------------------------------
Normal termination.
==1560==
==1560== HEAP SUMMARY:
==1560==     in use at exit: 72,704 bytes in 1 blocks
==1560==   total heap usage: 213 allocs, 212 frees, 94,519 bytes allocated
==1560==
==1560== LEAK SUMMARY:
==1560==    definitely lost: 0 bytes in 0 blocks
==1560==    indirectly lost: 0 bytes in 0 blocks
==1560==      possibly lost: 0 bytes in 0 blocks
==1560==    still reachable: 72,704 bytes in 1 blocks
==1560==         suppressed: 0 bytes in 0 blocks
==1560== Rerun with --leak-check=full to see details of leaked memory
==1560==
==1560== For counts of detected and suppressed errors, rerun with: -v
==1560== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)