_______________________________________________________________________________
TESTS FROM PS4
Output when you uncomment testPit() and testBoard() in main.cpp
Testing Pit
====================================
---Testing Pit constructor
Arguments: N, 1, N1, 4 nullptr, nullptr
-------Printing Pit-------
Owner: North
Number: 1
Label: N1
Next: nullptr
Opposite: nullptr
Contents: 4

---Testing Pit:scoop(). Expected: 4. Actual: 4
---Testing Pit:isEmpty(). Expected: 1. Actual: 1
---Testing Pit:drop(). Return nothing.
---Testing Pit:isEmpty(). Expected: 0. Actual: 0
---Testing Pit:printContents() Expected: 1. Actual: 1
---Testing Pit:isStore(). Expected: 0. Actual: 0


Testing Board
====================================
---Testing Board constructor
Arguments: 6 houses, 4 seeds

         N1    N2    N3    N4    N5    N6
        [ 4]  [ 4]  [ 4]  [ 4]  [ 4]  [ 4]
N0 [ 0]                                    [ 0] S0
         S6    S5    S4    S3    S2    S1
        [ 4]  [ 4]  [ 4]  [ 4]  [ 4]  [ 4]

--Test that opposite was set correctly
Pit: S6
Opposite: N1

Pit: S5
Opposite: N2

Pit: S4
Opposite: N3

Pit: S3
Opposite: N4

Pit: S2
Opposite: N5

Pit: S1
Opposite: N6

Pit: S0
Opposite: nullptr

Pit: N6
Opposite: S1

Pit: N5
Opposite: S2

Pit: N4
Opposite: S3

Pit: N3
Opposite: S4

Pit: N2
Opposite: S5

Pit: N1
Opposite: S6

Pit: N0
Opposite: nullptr


All tests passed.
