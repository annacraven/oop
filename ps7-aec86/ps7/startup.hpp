//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Header file for all Startup programs.
//  Created by Anna Craven on Dec. 4, 2019
// -----------------------------------------------------------------------------

#ifndef STARTUP_HPP
#define STARTUP_HPP

#include "HumanAgent.hpp"
#include "MachineAgent.hpp"
#include "agent.hpp"
#include "referee.hpp"

class Startup {
  public:
    Startup() = default;
    void go(int& argc, char**& argv);
    Agent* makeAgent(const string& agentType, Game* game, Player pl);
};

#endif