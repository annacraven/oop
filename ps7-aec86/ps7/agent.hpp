//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Header file for all Agent programs.
//  Created by Anna Craven on Nov 3, 2019
// -----------------------------------------------------------------------------
#ifndef AGENT_HPP
#define AGENT_HPP

#include "game.hpp"

class Agent {
  protected:
    const Game* game;
    const Player pl;
    const string type; // human or machine?
    Agent(const Game* _game, Player player, string t)
        : game(_game), pl(player), type(t) {}

  public:
    virtual ~Agent() = default;
    virtual int chooseMove() = 0;

    Player getPlayer() { return pl; }
    string getType() { return type; }
};

#endif