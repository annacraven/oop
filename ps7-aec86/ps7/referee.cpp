//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Implementation file for all Referee programs.
//  Created by Anna Craven on Oct. 27, 2019
// -----------------------------------------------------------------------------

#include "referee.hpp"

void Referee::playGame() {
    cout << "Welcome to Kalah!\n";
    while (not game.isOver()) {
        cout << "----------------------------------------------------------\n";
        cout << game.getActivePlayer() << "'s  turn:\n";
        int move = 0;

        while (not move) {
            try {
                move = current->chooseMove();
            } catch (NonIntMove) {
                try {
                    processCommand();
                } catch (QuitGame) {
                    return;
                } catch (InvalidCommandError& e) {
                    e.print(cout);
                } catch (Fatal& e) {
                    fatal(e.what());
                }
            } catch (InvalidMoveError& e) {
                cout << "Invalid move: ";
                e.print(cout) << endl;
            }
        }
        if (move) {
            processMove(move);
        }
    }

    game.endGame();
    showResults();
}

void Referee::processCommand() {
    string cmd;
    cin >> cmd;
    if (cin.fail()) {
        throw Fatal("Stream input error");
    }
    if (cmd == "q") {
        cout << "Quitting game at user's request\n";
        throw QuitGame();
    } else if (cmd == "z") {
        cout << "Undoing the most recent move\n";
        undo();
    } else if (cmd == "y") {
        cout << "Redoing the most recent move\n";
        redo();
    } else if (cmd == "s") {
        string file;
        cin >> file;
        cout << "Saving game to file " << file << "\n";
        doSave(file);
    } else if (cmd == "h") {
        cout << "-----Help-----\n";
        cout << "Possible commands:\n";
        cout << "\tz: Undo last move\n";
        cout << "\ty: Redo last move\n";
        cout << "\ts filename: Save game\n";
        cout << "\th: Help\n";
        cout << "\tq: Quit\n";
    } else {
        throw InvalidCommandError();
    }
}

void Referee::processMove(int move) {
    // clear redo stack
    redoStack.clear();
    // add snapshot to undo stack
    Snapshot s = game.saveState();
    undoStack.push_back(s);

    cout << game.getActivePlayer() << " playing move " << move << "\n";
    game.doMove(move);
    // check if there was a capture
    if (game.captured()) {
        cout << "Captured " << game.captured() << " seeds\n";
    }
    // check if player gets to choose a move again
    if (game.again()) {
        cout << game.getActivePlayer() << " gets another turn\n";
    } else {
        switchAgent();
        cout << "Turn is over\n";
    }
}

void Referee::showResults() {
    cout << "----------\n"
         << "Results:\n";
    game.printBoard();
    int n_score = game.score(N);
    int s_score = game.score(S);
    cout << "North's Score: " << n_score << "\n";
    cout << "South's Score: " << s_score << "\n";

    // display winner
    if (n_score > s_score) {
        cout << "North wins!\n";
    } else if (s_score > n_score) {
        cout << "South wins!\n";
    } else {
        cout << "Tie!\n";
    }
}

void Referee::switchAgent() {
    if (current == North) {
        current = South;
    } else {
        current = North;
    }
}

void Referee::undo() {
    if (not undoStack.empty()) {
        // push a new snapshot of the current game onto the redo stack
        redoStack.push_back(game.saveState());
        // pop state from undo stack and restore game to that state
        Snapshot s = undoStack.back();
        game.restoreState(s);
        undoStack.pop_back();
        // make sure the current agent is correct
        while (current->getPlayer() != s.getActivePlayer()) {
            switchAgent();
        }
    } else {
        cout << "Cannot undo; undo stack is empty\n";
    }
}

void Referee::redo() {
    if (not redoStack.empty()) {
        // push a new snapshot of the current game onto the udno stack
        undoStack.push_back(game.saveState());
        // pop state from redo stack and restore game to that state
        Snapshot s = redoStack.back();
        game.restoreState(s);
        redoStack.pop_back();
        // make sure the current agent is correct
        while (current->getPlayer() != s.getActivePlayer()) {
            switchAgent();
        }
    } else {
        cout << "Cannot redo; redo stack is empty\n";
    }
}

void Referee::doSave(string filename) const {
    ofstream fout(filename);
    if (!fout) {
        cout << "Unable to read file " << filename << endl;
        return;
    }
    fout << game.getNumHouses() << "\n";
    // what type of agent is South?
    fout << South->getType() << "\n";
    // what type of agent is North?
    fout << North->getType() << "\n";
    game.saveState().print(fout);
    fout.close();
}