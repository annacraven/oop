//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Header file for all Board programs. board.hpp Created by Anna Craven on Oct.
//  4, 2019
// -----------------------------------------------------------------------------

#ifndef BOARD_HPP
#define BOARD_HPP

#include "pit.hpp"

class Board {
  private:
    int numHouses;
    Pit* pits;
    int size; // size of pits array. 2 * (numHouses + 1)

  public:
    Board(){};
    Board(int _numHouses, int numSeeds) : numHouses(_numHouses) {
        size = 2 * (numHouses + 1);
        pits = new Pit[size];

        // keep track of which slot you are currently filling in in pits
        int i = 0;
        // create south houses
        while (i < numHouses) {
            int number = numHouses - i;
            string string_label = "S" + to_string(number);
            Pit* next = &pits[i + 1];
            Pit* opposite = &pits[size - 2 - i];
            pits[i] = Pit(S, number, string_label, numSeeds, next, opposite);
            i++;
        }
        // create south store. contents is 0, opposite is nullptr
        pits[i] = Pit(S, 0, "S0", 0, &pits[i + 1], nullptr);
        i++;
        // create north houses
        while (i < size - 1) {
            int number = size - i - 1;
            string string_label = "N" + to_string(number);
            Pit* next = &pits[i + 1];
            Pit* opposite = &pits[size - 2 - i];
            pits[i] = Pit(N, number, string_label, numSeeds, next, opposite);
            i++;
        }
        // create north store, and set its next to the start of the pits array
        pits[i] = Pit(N, 0, "N0", 0, &pits[0], nullptr);
    }
    Board(int _numHouses, vector<int> seeds) : numHouses(_numHouses) {
        size = 2 * (_numHouses + 1);
        pits = new Pit[size];

        // keep track of which slot you are currently filling in in pits
        int i = 0;
        // create south houses
        while (i < numHouses) {
            int number = numHouses - i;
            string string_label = "S" + to_string(number);
            Pit* next = &pits[i + 1];
            Pit* opposite = &pits[size - 2 - i];
            pits[i] = Pit(S, number, string_label, seeds[i], next, opposite);
            i++;
        }
        // create south store. contents is 0, opposite is nullptr
        pits[i] = Pit(S, 0, "S0", seeds[i], &pits[i + 1], nullptr);
        i++;
        // create north houses
        while (i < size - 1) {
            int number = size - i - 1;
            string string_label = "N" + to_string(number);
            Pit* next = &pits[i + 1];
            Pit* opposite = &pits[size - 2 - i];
            pits[i] = Pit(N, number, string_label, seeds[i], next, opposite);
            i++;
        }
        // create north store, and set its next to the start of the pits array
        pits[i] = Pit(N, 0, "N0", seeds[i], &pits[0], nullptr);
    }
    ~Board() { delete[] pits; }

    // find the pit given a player and a pit number
    Pit* findPit(Player player, int pitNum) const;
    // print the board in an attractive way
    ostream& print(ostream& out) const;
    // drop the the number of seeds into the given player's store
    void dropStore(Player player, int seeds);
    // returns the number of seeds in the given player's store
    int storeContents(Player player);
    // check if all the houses for the player are empty
    bool allEmpty(Player player);
    Pit* sow(Player player, int move);
    // performs capture and returns the number of captured seeds
    int doCapture(Player player, Pit* pit);
    void clearHouses();
    // returns true if the given number is a number of a house
    bool isValidHouse(int n) const { return n <= numHouses && n > 0; }
    int firstNonEmptyHouse(Player pl) const;

    // functions for debugging and testing
    ostream& debugPrint(ostream& out) const;
    void printPitAndOpposite() const;
    // puts the number of seeds in each pit into the vector
    void save(vector<int>& seeds) const;
    // fills in the pits with the given vector
    void set(const vector<int>& seeds) const;
};

// overload << operator to call Board's print function
inline ostream& operator<<(ostream& out, const Board& board) {
    return board.print(out);
}

#endif
