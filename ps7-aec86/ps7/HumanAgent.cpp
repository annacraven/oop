//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Implementation file for all HumanAgent programs.
//  Created by Anna Craven on Nov 3, 2019
// -----------------------------------------------------------------------------"
#include "HumanAgent.hpp"

int HumanAgent::chooseMove() {
    game->printBoard();
    int move = 0;
    int valid_move = 0;

    while (not valid_move) {
        cout << "Please enter a house number for " << pl;
        cout << " or a command letter z y s q h: ";
        cin >> move;
        if (cin.fail()) {
            cin.clear();
            throw NonIntMove();
        }
        try {
            game->isValidMove(move);
            valid_move = move;
        } catch (InvalidMoveError& e) {
            throw; // throw error for referee to catch
        }
    }

    return valid_move;
}