//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Header file for all Exception class.
//  Created by Anna Craven on Dec. 4, 2019
// -----------------------------------------------------------------------------
#ifndef EXCEPTION_HPP
#define EXCEPTION_HPP

class InvalidMoveError {
	string msg;

	public:
		InvalidMoveError(string s) : msg(s) {}
		ostream& print(ostream& out) const { return out << msg; }
};

class NonIntMove {

	public:
		NonIntMove() {}
		ostream& print(ostream& out) const { return out << "Non-interger move"; }
};

class InvalidCommandError {

	public:
		InvalidCommandError() {}
		ostream& print(ostream& out) const { return out << "Invalid command"; }
};

class QuitGame {

	public:
		QuitGame() {}
};



#endif