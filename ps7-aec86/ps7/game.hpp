//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Header file for all Game programs.
//  Created by Anna Craven on Oct. 27, 2019
// -----------------------------------------------------------------------------

#ifndef GAME_HPP
#define GAME_HPP

#include "Exception.hpp"
#include "board.hpp"
#include "player.hpp"
#include "snapshot.hpp"

class Game {
  private:
    Player activePlayer;      // the player whose turn it is
    Board board;              // a board on which to play the game
    int capturedSeeds = 0;    // if a capture was performed, how many seeds
    bool anotherTurn = false; // true if the active player gets to go again
    int numHouses;            // the number of houses in the board

  public:
    Game(){};
    Game(int _numHouses, int numSeeds)
        : board(_numHouses, numSeeds), numHouses(_numHouses) {
        activePlayer = S; // South goes first, by rules of Kalah
    }
    Game(int& _numHouses, vector<int>& seeds, string player)
        : board(_numHouses, seeds), numHouses(_numHouses) {
        if (player == "S") {
            activePlayer = S;
        } else if (player == "N") {
            activePlayer = N;
        } else {
            fatal("Invalid player type");
        }
    }

    bool isValidMove(int move) const;
    void doMove(int move);
    // returns true if the game is over
    // i.e., if all the houses of one player are empty
    bool isOver();
    void endGame();
    // creates snapshot of the current game state and adds it to the undo stack
    Snapshot saveState() const;
    // restores game to given snapshot
    void restoreState(const Snapshot& ss);
    // return the player whose turn it is
    Player getActivePlayer() const { return activePlayer; }
    // print board
    void printBoard() const { cout << board; }
    // get score for given player
    int score(Player p) { return board.storeContents(p); }
    // get number of captured seeds
    int captured() const { return capturedSeeds; }
    // check whether the current player gets to play again
    bool again() const { return anotherTurn; }
    int getFirstNonEmptyHouse(Player pl) const {
        return board.firstNonEmptyHouse(pl);
    }
    int getNumHouses() const { return numHouses; }
};

#endif