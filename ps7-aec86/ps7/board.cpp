// -----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Board class:  a board with pits, for a game of Kalah.
//  Implementation file for all Board programs.                  board.cpp
//  Created by Anna Craven on October 4, 2019
// -----------------------------------------------------------------------------
#include "board.hpp"

// print the board in the layout as though it were a physical board
ostream& Board::print(ostream& out) const {
    // print north houses
    out << "\n\t";
    for (int i = size - 2; i >= numHouses + 1; i--) {
        out << ' ' << pits[i].label() << ' ' << "  ";
    }
    // print north contents
    out << "\n\t";
    for (int i = size - 2; i >= numHouses + 1; i--) {
        out << "[ " << pits[i].getContents() << "]  ";
    }
    // print stores
    out << '\n';
    out << pits[size - 1].label() << " [ " << pits[size - 1].getContents()
        << ']';
    string gap(6 * numHouses, ' ');
    out << gap;
    out << "[ " << pits[numHouses].getContents() << "] "
        << pits[numHouses].label();

    // print south houses
    out << "\n\t";
    for (int i = 0; i < numHouses; i++) {
        out << ' ' << pits[i].label() << ' ' << "  ";
    }
    // print south contents
    out << "\n\t";
    for (int i = 0; i < numHouses; i++) {
        out << "[ " << pits[i].getContents() << "]  ";
    }

    out << '\n';
    return out;
}

Pit* Board::findPit(Player player, int pitNum) const {
    if (pitNum > numHouses) {
        return nullptr;
    }

    if (player == S) {
        // based on how pits[] was filled in
        return &pits[numHouses - pitNum];
    } else {
        return &pits[size - pitNum - 1];
    }
}

// seeds are scooped from the start pit,
// and dropped one at a time in the next pit
// returns the pit in which the last seed was dropped
Pit* Board::sow(Player player, int move) {
    // find the starting pit
    Pit* pit = findPit(player, move);
    // seeds are grabbed from starting pit
    int seeds = pit->scoop();
    // seeds are sown
    while (seeds > 0) {
        pit = pit->nextPit();
        // skip over opponent'ss store
        if (pit->isStore() && !pit->isOwnStore(player)) {
            continue;
        }
        pit->drop();
        seeds -= 1;
    }
    return pit;
}

// checks if conditions are met for capture
// if so, the appropriate pits are scooped and added to store
// @param pit [Pit] the pit in which the last seed was dropped
int Board::doCapture(Player player, Pit* pit) {
    // check if the pit was empty before dropping the last seed in
    if (pit->getContents() != 1) {
        return 0;
    }
    // check if the pit is a house that belongs to the player
    if (not pit->isOwnHouse(player)) {
        return 0;
    }
    // scoop seeds from that pit, and the opposite
    int seeds = pit->scoop();
    seeds += pit->oppositePit()->scoop();
    // drop seeds in player's store
    dropStore(player, seeds);
    return seeds;
}

void Board::dropStore(Player player, int seeds) {
    Pit* store = findPit(player, 0);
    store->add(seeds);
}

int Board::storeContents(Player player) {
    Pit* store = findPit(player, 0);
    return store->getContents();
}

// returns true if all the houses for the other player are empty
bool Board::allEmpty(Player player) {
    Pit* pit = findPit(!player, 0)->nextPit();
    while (not pit->isStore()) {
        if (not pit->isEmpty()) {
            return false;
        }
        pit = pit->nextPit();
    }
    return true;
}

// called when the game is over
// whichever player's houses are empty, the remaining pieces get moved
// to their opponents store
void Board::clearHouses() {
    // if south's houses are empty
    if (allEmpty(S)) {
        // scoop all of the seeds on North's side
        int seeds = 0;
        Pit* pit = findPit(S, 0)->nextPit();
        while (not pit->isStore()) {
            seeds += pit->scoop();
            pit = pit->nextPit();
        }
        // drop them into North's store
        dropStore(N, seeds);
    } else {
        // scoop all of the seeds on South's side
        int seeds = 0;
        Pit* pit = findPit(N, 0)->nextPit();
        while (not pit->isStore()) {
            seeds += pit->scoop();
            pit = pit->nextPit();
        }
        // drop them into North's store
        dropStore(S, seeds);
    }
}

int Board::firstNonEmptyHouse(Player pl) const {
    Pit* pit;
    int n = 1;
    while (n <= numHouses) {
        pit = findPit(pl, n);
        if (not pit->isEmpty()) {
            return n;
        }
        n++;
    }
    // all pits are empty
    return -1;
}

ostream& Board::debugPrint(ostream& out) const {
    out << "-----------Printing Board-----------\n";
    for (int i = 0; i < size; i++) {
        out << pits[i];
    }
    return out;
}

// function to test as the spec states
// "visit each house and print the label of the house and its opposite"
void Board::printPitAndOpposite() const {
    for (int i = 0; i < size; i++) {
        cout << "Pit: " << pits[i].label() << "\n";
        Pit* opp = pits[i].oppositePit();
        if (opp != nullptr) {
            cout << "Opposite: " << opp->label() << "\n\n";
        } else {
            cout << "Opposite: nullptr \n\n";
        }
    }
}

void Board::save(vector<int>& seeds) const {
    Pit* pit = findPit(N, 0)->nextPit();
    while (not pit->isOwnStore(N)) {
        seeds.push_back(pit->getContents());
        pit = pit->nextPit();
    }
    seeds.push_back(pit->getContents());
}

void Board::set(const vector<int>& seeds) const {
    Pit* pit = findPit(N, 0)->nextPit();
    for (auto i = seeds.begin(); i != seeds.end(); ++i) {
        pit->setContents(*i);
        pit = pit->nextPit();
    }
}
