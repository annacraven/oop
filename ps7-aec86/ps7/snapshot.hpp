//  ----------------------------------------------------------------------------
//  CPSC 427: Object-Oriented Programming
//  Header file for all Snapshot programs.
//  Created by Anna Craven on Nov. 27, 2019
// -----------------------------------------------------------------------------

#ifndef SNAPSHOT_HPP
#define SNAPSHOT_HPP

#include "player.hpp"
#include "tools.hpp"

class Snapshot {
    friend class Game;

  private:
    const Player activePlayer;
    const vector<int> seeds;

  public:
    Snapshot(Player pl, vector<int> s) : activePlayer(pl), seeds(s) {}
    ostream& print(ostream& out) const; // prints the snapshot to a file
    // I decided not to include this function, since it was optional
    // istream& read(istream& in); // reads a Snapshot from stream in

    Player getActivePlayer() { return activePlayer; }
};

#endif